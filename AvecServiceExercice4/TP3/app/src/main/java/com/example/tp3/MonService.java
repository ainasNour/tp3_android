package com.example.tp3;

import android.app.DownloadManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;

import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.URLUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MonService extends Service {
    private static final String TAG = "MonService";

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public MonService getService() {
            return MonService.this;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return  mBinder;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service démarré");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Service arrêté");
    }
    // Ajouter ici les méthodes que vous voulez exposer à votre activité ou fragment
    void downloadFile(String url) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setTitle("Téléchargement du fichier");
        request.setDescription("Téléchargement en cours...");
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        String fileName = URLUtil.guessFileName(url, null, null);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);

        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);

    }




// Maintenant, On parse le fichier

     String parseFile() {

         String name = null;
         String email = null;
         int age = 0;
         try {
             // Ouverture du fichier JSON dans un InputStream
             InputStream inputStream = getAssets().open("dataService.json");

             // Création d'un objet InputStreamReader à partir de l'InputStream
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

             // Création d'un objet BufferedReader pour lire le contenu du fichier JSON
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

             // Lecture du contenu du fichier JSON dans une chaîne
             StringBuilder stringBuilder = new StringBuilder();
             String line;
             while ((line = bufferedReader.readLine()) != null) {
                 stringBuilder.append(line);
             }
             String json = stringBuilder.toString();

             // Parsing du fichier JSON en un objet JSONObject
             JSONObject jsonObject = new JSONObject(json);

             // Récupération des données de l'utilisateur
             name = jsonObject.getString("name");
             age = jsonObject.getInt("age");
             email = jsonObject.getString("email");

             // Utilisation des données récupérées
             // ...

         } catch (IOException | JSONException e) {
             // Gestion des erreurs
             e.printStackTrace();
         }

         return "Nom :"+ name + "\n" + "Email:" + email + "Age:" + "\n" + age;
     }




}

