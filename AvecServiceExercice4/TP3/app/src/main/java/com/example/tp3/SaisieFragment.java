package com.example.tp3;

import android.app.DownloadManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SaisieFragment extends Fragment {

    EditText editTextNom;
    EditText editTextPrenom;
    static EditText editTextDateNaissance;
    static EditText editTextNumTel;
    static EditText editTextEmail;
    CheckBox checkBoxSport;
    CheckBox checkBoxMusique;
    CheckBox checkBoxLecture;
    RadioGroup radioGroupSynchro;
    RadioButton radioButtonOui;
    RadioButton radioButtonNon;
    Button buttonSubmit;
    Button buttonTelecharger;
    private static final String DOWNLOAD_URL = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
    private MonService mService;
private String TAG="SaisieFragment";
    private boolean mBound=false;
    private ServiceConnection mServiceConnection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1_layout, container, false);

        editTextNom = view.findViewById(R.id.editText_nom);
        editTextPrenom = view.findViewById(R.id.editText_prenom);
        editTextDateNaissance = view.findViewById(R.id.editText_dateNaissance);

        editTextNumTel = view.findViewById(R.id.editText_numTel);
        editTextEmail = view.findViewById(R.id.editText_email);

        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(10);
        editTextNumTel.setFilters(filters);

        String email = editTextEmail.getText().toString();
        if (!isValidEmail(email)) {
            editTextEmail.setError("Adresse e-mail invalide");
        }

        checkBoxSport = view.findViewById(R.id.checkBox_sport);
        checkBoxMusique = view.findViewById(R.id.checkBox_musique);
        checkBoxLecture = view.findViewById(R.id.checkBox_lecture);
        radioGroupSynchro = view.findViewById(R.id.radioGroup_synchro);
        radioButtonOui = view.findViewById(R.id.radioButton_oui);
        radioButtonNon = view.findViewById(R.id.radioButton_non);
        buttonSubmit = view.findViewById(R.id.button_submit);
        buttonTelecharger = view.findViewById(R.id.buttonTelecharger);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            // Définir une variable pour stocker l'instance de MonService

            @Override
            public void onClick(View v){
                String nom = editTextNom.getText().toString();
                String prenom = editTextPrenom.getText().toString();
                String dateNaissance = editTextDateNaissance.getText().toString();
                String numTel = editTextNumTel.getText().toString();
                String email = editTextEmail.getText().toString();

                StringBuilder interetsBuilder = new StringBuilder();
                if (checkBoxSport.isChecked()) {
                    interetsBuilder.append("Sport, ");
                }
                if (checkBoxMusique.isChecked()) {
                    interetsBuilder.append("Musique, ");
                }
                if (checkBoxLecture.isChecked()) {
                    interetsBuilder.append("Lecture, ");
                }
                String interets = interetsBuilder.toString();
                if (interets.endsWith(", ")) {
                    interets = interets.substring(0, interets.length() - 2);
                }

                boolean synchro = radioButtonOui.isChecked();
                if (!isValidPhoneNumber(numTel)) {
                    editTextNumTel.setError("Numéro de téléphone doit contenir 10 chiffres et commencer par un 0");
                } else if (!isValidEmail(email)) {
                    editTextEmail.setError("Adresse e-mail invalide");
                }else if(!isValidDate(dateNaissance,"dd/MM/yyyy")){
                    editTextDateNaissance.setError("La date de naissance doit être sous la forme jj/mm/aaaa");


                } else if (nom.isEmpty() || prenom.isEmpty() || dateNaissance.isEmpty() || numTel.isEmpty() || email.isEmpty()) {
                    Toast.makeText(getContext(), "Veuillez remplir tous les champs avant de confirmer", Toast.LENGTH_SHORT).show();
                } else {
                    AffichageFragment fragmentAffichage = new AffichageFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("nom", nom);
                    bundle.putString("prenom", prenom);
                    bundle.putString("dateNaissance", dateNaissance);
                    bundle.putString("numTel", numTel);
                    bundle.putString("email", email);
                    bundle.putString("interets", interets);
                    bundle.putBoolean("synchro", synchro);
                    fragmentAffichage.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(R.id.fragment_container, fragmentAffichage);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });

        // Gestion du clic sur le bouton Télécharger avec un service


        buttonTelecharger.setOnClickListener(new View.OnClickListener() {

            private void startAndBindService() {
                Intent intent = new Intent(getActivity(), MonService.class);
                getActivity().bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
            }

            @Override
            public void onClick(View v) {
                startAndBindService();
            }
        });

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                MonService.LocalBinder monBinder = (MonService.LocalBinder) binder;
                mService = monBinder.getService();
                mBound = true;
                Log.d("SaisieFragment", "Service lié");
                new DownloadTask().execute(DOWNLOAD_URL); // Appeler la méthode ici
            }
             class DownloadTask extends AsyncTask<String, Void, Void> {
                @Override
                protected Void doInBackground(String... urls) {
                    String url = urls[0];
                   mService.downloadFile(url);
                    return null;
                }}

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };





        return view;
    }
    @Override
    public void onDestroy () {
        super.onDestroy();
        if (mBound) {
            getContext().unbindService(mServiceConnection);
            mBound = false;
        }}

    private boolean isValidPhoneNumber(String phoneNumber) {
        Pattern pattern = Pattern.compile("^0\\d{9}$");

        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    public static boolean isValidDate(String inputDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.FRANCE);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inputDate.trim());
        } catch (ParseException | java.text.ParseException e) {
            return false;
        }
        return true;
    }



    public boolean isValidEmail(String email) {

        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{3,}$");

        return pattern.matcher(email).matches();
    }


    private void getDownloadDirectory() {
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String downloadPath = downloadDir.getPath();
        Log.d("Download directory", downloadPath);
    }


    public static void clearData() {
        // Supprimer les données qui ne doivent pas être conservées
       editTextNumTel.setText("");
       editTextEmail.setText("");
       editTextDateNaissance.setText("");
    }

}
