package com.example.tp3;


import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AffichageFragment extends Fragment {


    TextView textViewNom;
    TextView textViewPrenom;
    TextView textViewDateNaissance;
    TextView textViewNumTel;
    TextView textViewEmail;
    TextView textViewInterets;
    TextView textViewSynchro;
    Button buttonValider;
    Button button_parser;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2_layout, container, false);

        textViewNom = view.findViewById(R.id.textView_nom);
        textViewPrenom = view.findViewById(R.id.textView_prenom);
        textViewDateNaissance = view.findViewById(R.id.textView_dateNaissance);
        textViewNumTel = view.findViewById(R.id.textView_numTel);
        textViewEmail = view.findViewById(R.id.textView_email);
        textViewInterets = view.findViewById(R.id.textView_interets);
        textViewSynchro = view.findViewById(R.id.textView_synchro);
        buttonValider = view.findViewById(R.id.button_valider);
        button_parser = view.findViewById(R.id.button_parser);




        Bundle bundle = getArguments();
        String nom = bundle.getString("nom");
        String prenom = bundle.getString("prenom");
        String dateNaissance = bundle.getString("dateNaissance");
        String numTel = bundle.getString("numTel");
        String email = bundle.getString("email");
        String interets = bundle.getString("interets");
        boolean synchro = bundle.getBoolean("synchro");

        textViewNom.setText("Nom : " + nom);
        textViewPrenom.setText("Prénom : " + prenom);
        textViewDateNaissance.setText("Date de naissance : " + dateNaissance);
        textViewNumTel.setText("Numéro de téléphone : " + numTel);
        textViewEmail.setText("Adresse email : " + email);
        textViewInterets.setText("Centres d'intérêt : " + interets);
        textViewSynchro.setText("Synchronisation : " + (synchro ? "oui" : "non"));



        buttonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = textViewNom.getText().toString();
                String prenom = textViewPrenom.getText().toString();
                String dateNaissance = textViewDateNaissance.getText().toString();
                String NumTel = textViewNumTel.getText().toString();
                String Email = textViewEmail.getText().toString();
                String Interets = textViewInterets.getText().toString();
                String Synchro = textViewSynchro.getText().toString();

                try {
// Sauvegarde dans un fichier texte
                    FileOutputStream fileOutputStreamTxt = getActivity().openFileOutput("mon_fichier.txt", Context.MODE_PRIVATE);
                    OutputStreamWriter outputStreamWriterTxt = new OutputStreamWriter(fileOutputStreamTxt);
                    outputStreamWriterTxt.write(nom + "\n");
                    outputStreamWriterTxt.write(prenom + "\n");
                    outputStreamWriterTxt.write(dateNaissance + "\n");
                    outputStreamWriterTxt.write(NumTel + "\n");
                    outputStreamWriterTxt.write(Email + "\n");
                    outputStreamWriterTxt.write(Interets + "\n");
                    outputStreamWriterTxt.write(Synchro + "\n");
                    outputStreamWriterTxt.close();

                    // Création d'un objet JSON
                    JSONObject jsonObject = new JSONObject();

                    // Ajout des champs un par un
                    jsonObject.put("nom", nom.split(":\\s*")[1]);
                    jsonObject.put("prenom", prenom.split(":\\s*")[1]);
                    jsonObject.put("dateNaissance", dateNaissance.split(":\\s*")[1]);
                    jsonObject.put("NumTel", NumTel.split(":\\s*")[1]);
                    jsonObject.put("Email", Email.split(":\\s*")[1]);

                    // Création d'un tableau JSON pour les intérêts
                    JSONArray interetsArray = new JSONArray();
                    String[] interets = Interets.split(", ");
                    for (String interet : interets) {
                        interetsArray.put(interet.substring(interet.indexOf(":")+2)); // On extrait chaque élément avant la virgule

                    }
                    jsonObject.put("Interets", interetsArray);
                    jsonObject.put("Synchro", Synchro.split(":\\s*")[1]);

                    // Conversion de l'objet JSON en une chaîne JSON
                    String jsonString = jsonObject.toString();

                    // Ecriture de la chaîne JSON dans le fichier
                    FileOutputStream fileOutputStreamJson = getActivity().openFileOutput("mon_fichier.json", Context.MODE_PRIVATE);
                    OutputStreamWriter outputStreamWriterJson = new OutputStreamWriter(fileOutputStreamJson);
                    outputStreamWriterJson.write(jsonString);
                    outputStreamWriterJson.close();

                } catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                } catch (JSONException e) {
                    Log.e("Exception", "JSON conversion failed: " + e.toString());
                }
                getDownloadDirectory();
            }
        });

        button_parser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // Redirection vers l'activité ParserActivity
                Intent intent = new Intent(getActivity(), ParserActivity.class);
                startActivity(intent);
            }});



        return view;

    }
    private void getDownloadDirectory() {
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String downloadPath = downloadDir.getPath();
        Log.d("Download directory", downloadPath);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SaisieFragment.clearData();
    }


}