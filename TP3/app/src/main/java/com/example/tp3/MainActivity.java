package com.example.tp3;


import android.os.Bundle;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Chargement du premier fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SaisieFragment fragment1 = new SaisieFragment();
        fragmentTransaction.replace(R.id.fragment_container, fragment1);
        fragmentTransaction.commit();
    }

    // Fonction appelée depuis Fragment1 lors de la soumission des données
    public void onFragment1Submit() {
        // Chargement du deuxième fragment avec les données saisies
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        AffichageFragment fragment2 = new AffichageFragment();
        Bundle bundle = new Bundle();
        // bundle.putParcelable("data", data);
        fragment2.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container2, fragment2);
        fragmentTransaction.commit();
    }

    // Fonction appelée depuis Fragment2 lors de la validation des données
    public void onFragment2Validate() {
        // Effectuer les actions nécessaires après validation des données

    }
}

