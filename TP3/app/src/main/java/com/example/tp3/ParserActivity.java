package com.example.tp3;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;

public class ParserActivity extends AppCompatActivity {
    TextView nomValueTextView;
    TextView prenomValueTextView;
    TextView DateValueTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parser);
        AssetManager assetManager = getAssets();

        nomValueTextView = findViewById(R.id.nomValueTextView);

        prenomValueTextView = findViewById(R.id.prenomValueTextView);
        DateValueTextView=findViewById(R.id.DateValueTextView);


        InputStream inputStream = null;
        try {
            inputStream = assetManager.open("mon_fichier.json");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }

            JSONObject jsonObject = new JSONObject(stringBuilder.toString());

            String nom = jsonObject.getString("nom");
            String prenom = jsonObject.getString("prenom");
            String DateNaissance = jsonObject.getString("dateNaissance");

            nomValueTextView.setText(nom);
            prenomValueTextView.setText(prenom);
            DateValueTextView.setText(DateNaissance);


        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


    }

}
